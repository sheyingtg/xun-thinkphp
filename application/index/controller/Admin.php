<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/1/8 0008
 * Time: 8:46
 */

namespace app\index\controller;

use think\App;
use think\Controller;

class Admin extends Controller
{
    protected $search;
    protected $xs;

    public function __construct(App $app = null)
    {
        parent::__construct($app);
        require_once '/usr/local/xunsearch/sdk/php/lib/XS.php';
        $this->xs = new \XS('demo');
    }

    /**
     * 管理页面
     * @return \think\response\View
     */
    public function index()
    {
        return view();
    }

    /**
     * 添加文档
     */
    public function in()
    {
        return view();
    }

    /**
     * 添加
     */
    public function save()
    {
        $data['title']    = input('post.title');
        $data['id']       = input('post.id');
        $data['true_url'] = input('post.true_url');
        $data['views']    = input('post.views');
        $data['likenum']  = input('post.likenum');
        $data['litpic']   = input('post.litpic');
        $data['des']      = input('post.des');
        $data['addtime']  = strtotime(input('post.addtime'));

        $index = $this->xs->index; // 获取 索引对象
        // 创建文档对象
        $doc = new \XSDocument;
        $doc->setFields($data);
        // 添加到索引数据库中
        $index->add($doc);

        $this->success('添加成功');
    }

    /**
     * 更新文档
     * @return \think\response\View
     */
    public function up()
    {
        return view();
    }

    /**
     * 更新文档
     */
    public function update()
    {
        $data['title']    = input('post.title');
        $data['id']       = input('post.id');
        $data['true_url'] = input('post.true_url');
        $data['views']    = input('post.views');
        $data['likenum']  = input('post.likenum');
        $data['litpic']   = input('post.litpic');
        $data['des']      = input('post.des');
        $data['addtime']  = strtotime(input('post.addtime'));

        $index = $this->xs->index; // 获取 索引对象
        // 创建文档对象
        $doc = new \XSDocument;
        $doc->setFields($data);
        // 添加到索引数据库中
        $index->update($doc);

        $this->success('更新成功');
    }

    /**
     * 删除文档
     * @return \think\response\View
     */
    public function del()
    {
        return view();
    }

    /**
     * 删除文档操作
     */
    public function eliminate()
    {
        $id = input('post.id');
        if(!$id) {
            $this->error('请输入主键ID');
        }
        $data = explode("\n", $id);

        $index = $this->xs->index; // 获取 索引对象

        foreach($data as $k => $v) {
            $index->del($v);  // 删除主键值为 123 的记录
        }
        $this->success('删除成功');
    }

    /**
     * 清空索引
     * 清空索引是一个同步操作，一旦执行立即生效，并且不可恢复。
     * 如果采用这种 方式重建索引，由于原有索引被立即清空了，可能会有一小段时间无法搜索到数据。
     * 因此，如果您对线上项目操作，建议参见下一章节中介绍的平滑方式重建索引。
     */
    public function clear()
    {

        $index = $this->xs->index; // 获取 索引对象
        // 执行清空操作
        $index->clean();
        $this->success('清空成功');
    }

    /**
     * 平滑重建索引
     */
    public function rebuild()
    {
        $index = $this->xs->index; // 获取 索引对象
        // 宣布开始重建索引
        $index->beginRebuild();
        // 然后在此开始添加数据
        $data = [];
        $index->add($data);
        // 告诉服务器重建完比
        $index->endRebuild();
    }

    /**
     * 获取热门搜索词
     * @return \think\response\View
     */
    public function hot()
    {
        $num  = input('get.num') ? input('get.num') : 6;
        $type = input('get.type') ? input('get.type') : 'total';
        $search = $this->xs->search; // 获取 搜索对象
        $hot    = $search->getHotQuery($num, $type); // 获取前 6 个(默认)总热门搜索词，最大50个
        return view('', [ 'hot' => $hot ]);
    }
}
