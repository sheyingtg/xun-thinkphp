<?php

namespace app\index\controller;

use think\Controller;

class Index extends Controller
{
    /**
     * 搜索
     * @return \think\response\View
     */
    public function index()
    {
        return view('/index');
    }
}
